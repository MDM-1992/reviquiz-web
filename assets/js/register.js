import Vue from 'vue';

const app = new Vue({
    el: '#app',
    data: {
        errors: [],
        name: null,
        prenom: null,
        login: null,
        classe: null,
        etablissement: null,
        email: null,
        password: null

    },
    methods:{
        checkForm: function (e) {
            if (this.name && this.prenom && this.login && this.classe && this.etablissement && this.email && this.password) {
                return true;
            }

            this.errors = [];

            if (!this.name) {
                this.errors.push('Name obligatoire.');
            }
            if (!this.prenom) {
                this.errors.push('Prenom obligatoire.');
            }
            if (!this.login) {
                this.errors.push('Login obligatoire.');
            }
            if (!this.classe) {
                this.errors.push('Classe obligatoire.');
            }
            if (!this.etablissement) {
                this.errors.push('Etablissement obligatoire.');
            }
            if (!this.email) {
                this.errors.push('Email obligatoire.');
            }
            if (!this.password) {
                this.errors.push('Password obligatoire.');
            }

            e.preventDefault();
        }
    }
})