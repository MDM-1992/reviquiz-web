<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Question
 *
 * @ORM\Table(name="question", indexes={@ORM\Index(name="foreignKey_Questionnaire", columns={"idQuestionnaire"})})
 * @ORM\Entity
 */
class Question
{
    /**
     * @var int
     *
     * @ORM\Column(name="idQuestion", type="integer", nullable=false, options={"comment"="ID de la question"})
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $idquestion;

    /**
     * @var string
     *
     * @ORM\Column(name="libelleQuestion", type="text", length=65535, nullable=false, options={"comment"="Texte de la question"})
     */
    private $libellequestion;

    /**
     * @var int
     *
     * @ORM\Column(name="numQuestion", type="integer", nullable=false, options={"comment"="Numero de la question dans le questionnaire"})
     */
    private $numquestion;

    /**
     * @var \Questionnaire
     *
     * @ORM\ManyToOne(targetEntity="Questionnaire")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="idQuestionnaire", referencedColumnName="idQuestionnaire")
     * })
     */
    private $idquestionnaire;

    public function getIdquestion(): ?int
    {
        return $this->idquestion;
    }

    public function getLibellequestion(): ?string
    {
        return $this->libellequestion;
    }

    public function setLibellequestion(string $libellequestion): self
    {
        $this->libellequestion = $libellequestion;

        return $this;
    }

    public function getNumquestion(): ?int
    {
        return $this->numquestion;
    }

    public function setNumquestion(int $numquestion): self
    {
        $this->numquestion = $numquestion;

        return $this;
    }

    public function getIdquestionnaire(): ?Questionnaire
    {
        return $this->idquestionnaire;
    }

    public function setIdquestionnaire(?Questionnaire $idquestionnaire): self
    {
        $this->idquestionnaire = $idquestionnaire;

        return $this;
    }


}
