<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Historiqueqcm
 *
 * @ORM\Table(name="historiqueqcm", indexes={@ORM\Index(name="foreignKey_Etudiant", columns={"idEtudiant"}), @ORM\Index(name="dateQCM", columns={"date"}), @ORM\Index(name="idQuestionnaire", columns={"idQuestionnaire"})})
 * @ORM\Entity
 */
class Historiqueqcm
{
    /**
     * @var int
     *
     * @ORM\Column(name="idHisto", type="integer", nullable=false, options={"comment"="ID de l'histo"})
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $idhisto;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date", type="date", nullable=false, options={"comment"="Date où le QCM a été fait"})
     */
    private $date;

    /**
     * @var int
     *
     * @ORM\Column(name="resultat", type="integer", nullable=false, options={"comment"="Nombre de bonnes réponses de l'étudiant"})
     */
    private $resultat;

    /**
     * @var \User
     *
     * @ORM\ManyToOne(targetEntity="User")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="idEtudiant", referencedColumnName="id")
     * })
     */
    private $idetudiant;

    /**
     * @var \Questionnaire
     *
     * @ORM\ManyToOne(targetEntity="Questionnaire")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="idQuestionnaire", referencedColumnName="idQuestionnaire")
     * })
     */
    private $idquestionnaire;

    public function getIdhisto(): ?int
    {
        return $this->idhisto;
    }

    public function getDate(): ?\DateTimeInterface
    {
        return $this->date;
    }

    public function setDate(\DateTimeInterface $date): self
    {
        $this->date = $date;

        return $this;
    }

    public function getResultat(): ?int
    {
        return $this->resultat;
    }

    public function setResultat(int $resultat): self
    {
        $this->resultat = $resultat;

        return $this;
    }

    public function getIdetudiant(): ?User
    {
        return $this->idetudiant;
    }

    public function setIdetudiant(?User $idetudiant): self
    {
        $this->idetudiant = $idetudiant;

        return $this;
    }

    public function getIdquestionnaire(): ?Questionnaire
    {
        return $this->idquestionnaire;
    }

    public function setIdquestionnaire(?Questionnaire $idquestionnaire): self
    {
        $this->idquestionnaire = $idquestionnaire;

        return $this;
    }


}
