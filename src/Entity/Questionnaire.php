<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Questionnaire
 *
 * @ORM\Table(name="questionnaire", indexes={@ORM\Index(name="foreignKey_Niveau", columns={"idClasse"}), @ORM\Index(name="foreignKey_Matiere", columns={"idMatiere"})})
 * @ORM\Entity
 */
class Questionnaire
{

    /**
     * @var int
     *
     * @ORM\Column(name="idQuestionnaire", type="integer", nullable=false, options={"comment"="ID du questionnaire"})
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $idquestionnaire;

    /**
     * @var string
     *
     * @ORM\Column(name="libelle", type="string", length=50, nullable=false, options={"comment"="Nom du questionnaire"})
     */
    private $libelle;

    /**
     * @var int
     *
     * @ORM\Column(name="nbQuestions", type="integer", nullable=false, options={"comment"="Nombre de questions du questionnaire"})
     */
    private $nbquestions;

    /**
     * @var \Classe
     *
     * @ORM\ManyToOne(targetEntity="Classe")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="idClasse", referencedColumnName="id_classe")
     * })
     */
    private $idclasse;

    /**
     * @var \Matiere
     *
     * @ORM\ManyToOne(targetEntity="Matiere")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="idMatiere", referencedColumnName="idMatiere")
     * })
     */
    private $idmatiere;


    public function getIdquestionnaire(): ?int
    {
        return $this->idquestionnaire;
    }

    public function setIdquestionnaire(int $idquestionnaire): self
    {
        $this->idquestionnaire = $idquestionnaire;

        return $this;
    }

    public function getLibelle(): ?string
    {
        return $this->libelle;
    }

    public function setLibelle(string $libelle): self
    {
        $this->libelle = $libelle;

        return $this;
    }

    public function getNbquestions(): ?int
    {
        return $this->nbquestions;
    }

    public function setNbquestions(int $nbquestions): self
    {
        $this->nbquestions = $nbquestions;

        return $this;
    }

    public function getIdclasse(): ?Classe
    {
        return $this->idclasse;
    }

    public function setIdclasse(?Classe $idclasse): self
    {
        $this->idclasse = $idclasse;

        return $this;
    }

    public function getIdmatiere(): ?Matiere
    {
        return $this->idmatiere;
    }

    public function setIdmatiere(?Matiere $idmatiere): self
    {
        $this->idmatiere = $idmatiere;

        return $this;
    }


}
