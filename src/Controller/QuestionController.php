<?php

namespace App\Controller;

use App\Entity\Question;
use App\Entity\Questionnaire;
use App\Entity\Historiqueqcm;
use App\Entity\User;

use App\Form\QuestionType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\User\UserInterface;

/**
 * @Route("/question")
 */
class QuestionController extends AbstractController
{
    /**
     * @Route("/show/{id}", name="question_show", methods={"GET"})
     */
    public function show(Question $question): Response
    {
        return $this->render('question/show.html.twig', [
            'question' => $question,
        ]);
    }

    /**
     * @Route("/{id}", name="question_index", methods={"GET"})
     */
    public function index(Questionnaire $questionnaire): Response
    {

        $questions = $this->getDoctrine()
            ->getRepository(Question::class)
            ->findby(['idquestionnaire'=>$questionnaire->getIdquestionnaire()]);

        return $this->render('question/index.html.twig', [
            'questions' => $questions,
        ]);
    }

    /**
     * @Route("/verificationQuiz", name="question_verif", methods={"POST"})
     */
    public function verificationQuiz(UserInterface $user): Response
    {
        $idquestionnaireEnCours = $_POST['idquestionnaire'];

        //On etablit les infos de connexion a la base de donnée
        $link = mysqli_connect("localhost", "root", "", "reviquiz");

        //On verifie que la connexion a la base a réussie, sinon message d'erreur
        if (mysqli_connect_errno()) {
            printf("Échec de la connexion : %s\n", mysqli_connect_error());
            exit();
        }

        //recuperation de l'enreg questionnaire en cours
        if ($result = mysqli_query($link, "SELECT * FROM questionnaire WHERE idQuestionnaire = $idquestionnaireEnCours ")) {
            /* Recuperation puis Libération du jeu de résultats */
            $questionnaire = $result->fetch_assoc();
            mysqli_free_result($result);
        }

        $count = 0;
        $bonnereponse = 0;
        $questionstotales = $questionnaire["nbQuestions"];
        for ($i = 1; $i <= 20; $i++){
            if(isset($_POST["question_$i"])){
                $count = $count + 1;
                $reponsechoisie = $_POST["question_$i"];
                if ($result = mysqli_query($link, "SELECT valeur FROM reponse WHERE idReponse = $reponsechoisie ")) {
                    $reponse = $result->fetch_assoc();
                    if($reponse["valeur"] == "1"){
                        $bonnereponse = $bonnereponse + 1;
                    }
                    mysqli_free_result($result);
                }
            }
        }

            $repository = $this->getDoctrine()
                ->getManager()
                ->getRepository(Questionnaire::class);
            $questionnaireHisto = $repository->find($idquestionnaireEnCours);

        $histo = new Historiqueqcm();
        $histo->setIdetudiant($this->getUser());
        $histo->setDate(new \DateTime());
        $histo->setIdquestionnaire($questionnaireHisto);
        $histo->setResultat($bonnereponse);
        $em = $this->getDoctrine()->getManager();
        $em->persist($histo);
        $em->flush();

        return $this->render('question/resultats.html.twig', [
            'count' => $count,
            'nbbonnesreponses' => $bonnereponse,
            'nbquestiontotale' => $questionstotales,
        ]);

        //Fermeture de la connexion avec la base de donnée
        mysqli_close($link);
    }



}
