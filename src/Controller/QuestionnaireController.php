<?php

namespace App\Controller;

use App\Entity\Questionnaire;
use App\Form\QuestionnaireType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;


/**
 * @Route("/questionnaire")
 */
class QuestionnaireController extends AbstractController
{
    /**
     * @Route("/", name="questionnaire_index", methods={"GET"})
     */
    public function index(): Response
    {
        $questionnaires = $this->getDoctrine()
            ->getRepository(Questionnaire::class)
            ->findBy(['idclasse'=>$this->getUser()->getIdClasse()->getIdClasse()]);

        return $this->render('questionnaire/index.html.twig', [
            'questionnaires' => $questionnaires,
        ]);
    }

    /**
     * @Route("/{id}", name="questionnaire_show", methods={"GET"})
     */
    public function show(Questionnaire $questionnaire): Response
    {
        return $this->render('questionnaire/show.html.twig', [
            'questionnaire' => $questionnaire,
        ]);
    }
}
