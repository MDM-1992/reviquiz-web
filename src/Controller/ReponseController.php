<?php

namespace App\Controller;

use App\Entity\Reponse;
use App\Form\ReponseType;
use App\Entity\Question;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/reponse")
 */
class ReponseController extends AbstractController
{
    /**
     * @Route("/{id}", name="reponse_index", methods={"GET"})
     */
    public function index(Question $question): Response
    {
        $reponses = $this->getDoctrine()
            ->getRepository(Reponse::class)
            ->findBy(['idquestion'=>$question->getIdQuestion()]);
            dump($reponses);
        return $this->render('reponse/index.html.twig', [
            'reponses' => $reponses,
            'question'=>$question,
        ]);
    }


    /**
     * @Route("/{idreponse}", name="reponse_show", methods={"GET"})
     */
    public function show(Reponse $reponse): Response
    {
        return $this->render('reponse/show.html.twig', [
            'reponse' => $reponse,
        ]);
    }
}
