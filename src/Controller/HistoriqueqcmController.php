<?php

namespace App\Controller;

use App\Entity\Historiqueqcm;
use App\Form\HistoriqueqcmType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/historiqueqcm")
 */
class HistoriqueqcmController extends AbstractController
{
    /**
     * @Route("/", name="historiqueqcm_index", methods={"GET"})
     */
    public function index(): Response
    {
        $historiqueqcms = $this->getDoctrine()
            ->getRepository(Historiqueqcm::class)
            ->findby(['idetudiant'=>$this->getUser()]);

        return $this->render('historiqueqcm/index.html.twig', [
            'historiqueqcms' => $historiqueqcms,
        ]);
    }

    /**
     * @Route("/new", name="historiqueqcm_new", methods={"GET","POST"})
     */
    public function new(Request $request): Response
    {
        $historiqueqcm = new Historiqueqcm();
        $form = $this->createForm(HistoriqueqcmType::class, $historiqueqcm);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($historiqueqcm);
            $entityManager->flush();

            return $this->redirectToRoute('historiqueqcm_index');
        }

        return $this->render('historiqueqcm/new.html.twig', [
            'historiqueqcm' => $historiqueqcm,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{idhisto}", name="historiqueqcm_show", methods={"GET"})
     */
    public function show(Historiqueqcm $historiqueqcm): Response
    {
        return $this->render('historiqueqcm/show.html.twig', [
            'historiqueqcm' => $historiqueqcm,
        ]);
    }

    /**
     * @Route("/{idhisto}/edit", name="historiqueqcm_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, Historiqueqcm $historiqueqcm): Response
    {
        $form = $this->createForm(HistoriqueqcmType::class, $historiqueqcm);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('historiqueqcm_index');
        }

        return $this->render('historiqueqcm/edit.html.twig', [
            'historiqueqcm' => $historiqueqcm,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{idhisto}", name="historiqueqcm_delete", methods={"DELETE"})
     */
    public function delete(Request $request, Historiqueqcm $historiqueqcm): Response
    {
        if ($this->isCsrfTokenValid('delete'.$historiqueqcm->getIdhisto(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($historiqueqcm);
            $entityManager->flush();
        }

        return $this->redirectToRoute('historiqueqcm_index');
    }
}
